#include <math.h>

#define COROUTINE(...) int _cojump = 0; bool operator()(__VA_ARGS__) { switch(_cojump) { case 0:
#define YIELD() _cojump = __LINE__; return true; case __LINE__: 
#define FINISH() _cojump = 0; } return false; }
#define AWAIT(x) while(x) { YIELD(); }

void key_down(byte k, byte v);
void key_up(byte k, byte v);
void control(byte id, byte dx);

struct co_delay
{   uint32_t start;
    COROUTINE(uint32_t usec);
    start = micros();
    while(micros() - start < usec) { YIELD(); }
    FINISH();
};

namespace key {

    constexpr int keys = 37;
    constexpr int midi_offset = 48;
    
    // pins
    constexpr int drive_pin = 26;
    constexpr int drive_len = 7;
    constexpr int read_pin = 0;
    constexpr int read_len = 12;
    
    uint32_t key_time[keys] = {};
    uint8_t key_state[keys] = {};
    
    void setup()
    {   for(int i=0 ; i<read_len ; i++)
        {   pinMode(read_pin + i, INPUT_PULLUP);
        }
        for(int i=0 ; i<drive_len ; i++)
        {   pinMode(drive_pin + i, OUTPUT);
            digitalWrite(drive_pin + i, 1); // active low
        }
    }
    void down(int k)
    {   uint32_t dt = micros() - key_time[k];
        if(dt > 100000) { dt = 100000; }
        if(dt < 2000) { dt = 2000; }
        int v = 254000 / dt;
        key_down(midi_offset + k, v);
    }
    void up(int k)
    {   key_up(midi_offset + k, 0);
    }
    void update(int k, bool sw0, bool sw1)
    {   uint8_t & st = key_state[k];
        switch(st)
        {   case 0:
                if(sw0||sw1) { key_time[k] = micros(); st = 1; }
                break;
            case 1:
                if(sw1) { key::down(k); st = 2; }
                else if(!sw0) { st = 0; }
                break;
            case 2:
                if(!sw1) { st = 3; }
                break;
            default:
                if(!sw0) { key::up(k); st = 0; }
                break;
        }
    }

    struct loop
    {   co_delay delay;
        int d;
        COROUTINE();
        setup();
        while(1)
        {   for(d=0 ; d<drive_len ; d++)
            {   digitalWrite(drive_pin + d, 0); // active low
                AWAIT(delay(10));
                for(int i=0 ; i<read_len/2 ; i++)
                {   int k = d * 6 - i;
                    if(k<0 || k>=keys) { continue; }
                    int sw0 = !digitalRead(read_pin + i*2 + 1);
                    int sw1 = !digitalRead(read_pin + i*2);
                    update(k, sw0, sw1);
                }
                digitalWrite(drive_pin + d, 1);
            }
        }
        FINISH();
    };

} // key

namespace knob {
        
    constexpr byte clk_pin = 23;
    constexpr byte dat_pin = 18;
    constexpr int devs = 5;
    constexpr byte addr = 53;
    constexpr int readlen = 7;
    constexpr uint32_t dt = 5; // microseconds per half-clock
    
    constexpr byte init[2][2] = {
        {0x10, 0xa9}, // x-y-z enable, adc after read, x2 sens, parity
        {0x11, 0x15} // 1 byte read, no INT, master control mode
    };

    constexpr bool ACK = 0;
    constexpr bool NACK = 1;
    constexpr bool WRITE = 0;
    constexpr bool READ = 1;

    bool pin(int pin, bool up)
    {   if(up)
        {   pinMode(pin, INPUT_PULLUP);
            digitalWrite(pin, 1); // TODO required?
            return digitalRead(pin) == 1;
        }
        else
        {   digitalWrite(pin, 0);
            pinMode(pin, OUTPUT);
            return digitalRead(pin) == 0;
        }
    }
    bool clk(bool up)
    {   return pin(clk_pin, up);
    }
    void write_all(bool up)
    {   for(int idx=0 ; idx<devs ; idx++)
        {   pin(dat_pin + idx, up);
        }
    }
    bool read_bit(int idx)
    {   return digitalRead(dat_pin + idx);
    }

    struct co_send
    {   co_delay delay;
        int i;

        COROUTINE(byte data);
        
        for(i=7 ; i>=0 ; i--)
        {   clk(0);
            write_all(data&(1<<i));
            AWAIT(delay(dt));
            clk(1);
            AWAIT(delay(dt));
        }
        clk(0);
        write_all(1);
        AWAIT(delay(dt));
        clk(1);
        AWAIT(delay(dt));
//        for(int j=0 ; j<devs ; j++)
//        {   bool ack = (read_bit(j) == ACK);
//            Serial.printf("%d: %s\n", j, ack?"ack":"nack");
//        }
        
        FINISH();
    };
    
    struct co_start
    {   co_delay delay;
        co_send send;
        int i;
        COROUTINE(byte addr, bool mode);
        clk(1);
        AWAIT(delay(dt));
        write_all(0);
        AWAIT(delay(dt));
        AWAIT(send((addr<<1)|mode));
        FINISH();
    };

    struct co_recv
    {   co_delay delay;
        int i;

        COROUTINE(byte * bytes, int devpitch);
        
        for(int j=0 ; j<devs ; j++)
        {   bytes[j * devpitch] = 0;
        }
        for(i=7 ; i>=0 ; i--)
        {   clk(0);
            write_all(1);
            AWAIT(delay(dt));
            clk(1);
            AWAIT(delay(dt));
            for(int j=0 ; j<devs ; j++)
            {   bytes[j * devpitch] |= (read_bit(j)<<i);
            }
        }

        clk(0);
        write_all(ACK);
        AWAIT(delay(dt));
        clk(1);
        AWAIT(delay(dt));
        
        FINISH();
    };
    
    struct co_finish
    {   co_delay delay;
        int i;

        COROUTINE();
        
        // stop
        clk(0);
        write_all(0);
        AWAIT(delay(dt));
        clk(1);
        AWAIT(delay(dt));
        write_all(1);
        AWAIT(delay(dt));
        
        FINISH();
    };

    struct co_send_bytes
    {   co_start start;
        co_send send;
        co_finish finish;
        int i;

        COROUTINE(byte dev_addr, byte const* bytes, int nbytes);
        AWAIT(start(dev_addr, WRITE));
        for(i=0 ; i<nbytes ; i++)
        {   AWAIT(send(bytes[i]));
        }
        AWAIT(finish());
        FINISH();
    };

    struct co_recv_bytes
    {   co_start start;
        co_recv recv;
        co_finish finish;
        int i;
        
        // byte bytes[ndevs][nbytes]
        COROUTINE(byte dev_addr, byte * bytes, int nbytes);

        AWAIT(start(dev_addr, READ));
        for(i=0 ; i<nbytes ; i++)
        {   AWAIT(recv(bytes+i, nbytes));
        }
        AWAIT(finish());
        
        FINISH();
    };

    struct loop
    {   co_delay delay;
        co_send_bytes send;
        co_recv_bytes recv;
        byte data[devs][readlen] = {};

        COROUTINE();
        AWAIT(delay(100000));
        // sensor reset sequence
        AWAIT(send(0xff, nullptr, 0));
        AWAIT(send(0xff, nullptr, 0));
        AWAIT(send(0x00, nullptr, 0));
        AWAIT(send(0x00, nullptr, 0));
        AWAIT(delay(40));
        AWAIT(send(addr, init[0], 2));
        AWAIT(send(addr, init[1], 2));
        AWAIT(delay(100000));

//        AWAIT(recv(addr, (byte*)data, readlen));
//        for(int i=0 ; i<devs ; i++)
//        {   for(int j=0 ; j<devs ; j++)
//            {   Serial.printf("%d:%d %2x\n",i,j,data[i][j]);
//            }
//        }
        
        while(1)
        {               
//            YIELD();
            AWAIT(delay(100000));

            AWAIT(recv(addr, (byte*)data, readlen));

            int n = 1;
            int32_t ix = (int8_t)data[n][0];
            int32_t iy = (int8_t)data[n][1];
            int32_t iz = (int8_t)data[n][2];
            ix = (ix<<4) | (data[n][4] >> 4);
            iy = (iy<<4) | (data[n][4] & 0xf);
            iz = (iz<<4) | (data[n][5] & 0xf);
        //    Serial.printf("x=%d\n", x);
        //    Serial.printf("y=%d\n", y);
        
            double x = ix;
            double y = iy;
            
            double a = atan2(y, x);
            Serial.printf("%lf\n", a);
            
            
        }
        FINISH();
    };
    
} // knob


void key_down(byte k, byte v)
{   Serial.printf("on %d %d\n", k, v);
    usbMIDI.sendNoteOn(k, v, 0);
}
void key_up(byte k, byte v)
{   Serial.printf("off %d %d\n", k, v);
    usbMIDI.sendNoteOff(k, v, 0);   
}
void control(byte id, byte dx)
{   Serial.printf("ctl %d %d\n", id, dx);
    usbMIDI.sendControlChange(id, 64+dx, 0);
}


key::loop check_keys;
knob::loop check_knobs;

void setup()
{   
    Serial.begin(9600);
    delay(100);
}

void loop()
{   
    check_keys();
    check_knobs();

    while(usbMIDI.read()) {}
    delayMicroseconds(5);
}
